# NLP_Road

### Objective

 - 培养AI思维，建模能力
 - 扎实的AI基础，杜绝只做基础调参
 - 解决问题能力
 - 通过项目深入理解NLP核心技术
 - 
 
### Process

 - Learn：看视频课程
 - Read：养成读文章（英文）的习惯，接触课程延伸的内容
 - Code：完成项目，自己完成，超过一万行AI程序保证有本质提升
 - Write：养成写文章习惯，梳理思路，自我总结
 - Discuss：群里多交流，不怕提出低级问题
 - Collaboration：鼓励项目合作
 
 
